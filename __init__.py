from trytond.pool import Pool

from .opencell_somconnexio.tryton_models.opencell_configuration import OpenCellConfiguration
from .opencell_somconnexio.tryton_models.opencell_one_shot_charge_codes import OpenCellOneShotChargeCodes
from .opencell_somconnexio.tryton_models.opencell_service_codes import OpenCellServiceCodes
from .opencell_somconnexio.tryton_models.wizards.opencell_contract_migration.opencell_contract_migration \
    import OpenCellContractMigration
from .opencell_somconnexio.tryton_models.wizards.opencell_contract_migration.start import OpenCellContractMigrationStart
from .opencell_somconnexio.tryton_models.opencell_tax_codes import OpenCellTaxCodes
from .opencell_somconnexio.tryton_models.opencell_invoice import OpenCellInvoice
from .opencell_somconnexio.tryton_models.opencell_invoice_account_move_report import OpenCellInvoiceAccountMoveReport
from .opencell_somconnexio.tryton_models.opencell_invoice_taxes_account_move_report \
    import OpenCellInvoiceTaxesAccountMoveReport
from .opencell_somconnexio.tryton_models.wizards.opencell_invoice_poll.opencell_invoice_poll \
    import OpenCellInvoicePoll
from .opencell_somconnexio.tryton_models.wizards.opencell_invoice_poll.start import OpenCellInvoicePollStart
from .opencell_somconnexio.tryton_models.invoice_list_polling import InvoiceListPollingModel

from .opencell_somconnexio.tryton_models.account_move import AccountMove
from .opencell_somconnexio.tryton_models.contract import Contract
from .opencell_somconnexio.tryton_models.contract_line import ContractLine

from .opencell_somconnexio.version import VERSION

import os
import bugsnag

api_key = os.getenv('BUGSNAG_OPENCELL_SOMCONNEXIO_TRYTON_API_KEY')
bugsnag.configure(api_key=api_key, app_version=VERSION)


def register():
    Pool.register(OpenCellConfiguration,
                  OpenCellOneShotChargeCodes,
                  OpenCellServiceCodes,
                  OpenCellContractMigrationStart,
                  InvoiceListPollingModel,
                  OpenCellInvoicePollStart,
                  OpenCellTaxCodes,
                  OpenCellInvoice,
                  OpenCellInvoiceAccountMoveReport,
                  OpenCellInvoiceTaxesAccountMoveReport,
                  AccountMove,
                  Contract,
                  ContractLine,
                  module="opencell_somconnexio",
                  type_="model")

    Pool.register(OpenCellContractMigration,
                  OpenCellInvoicePoll,
                  module="opencell_somconnexio",
                  type_="wizard")
