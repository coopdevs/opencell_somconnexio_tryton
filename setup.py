# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

from opencell_somconnexio.version import VERSION

README = open('README.md').read()

setup(
    name='opencell_somconnexio',
    version=VERSION,
    author='Coopdevs',
    author_email='info@coopdevs.org',
    maintainer='Daniel Palomar, Antonio Barcia',
    url='https://gitlab.com/coopdevs/opencell_somconnexio_tryton',
    description='Tryton module for Somconnexio data syncing in OpenCell',
    long_description=README,
    long_description_content_type='text/markdown',
    packages=find_packages(exclude=('tests', 'docs')),
    include_package_data=True,
    zip_safe=False,
    install_requires=['pyopencell', 'pyotrs', 'trytond==3.8.18', 'celery==4.3.0', 'celery-tryton==0.3'],
    test_suite='unittest2.collector',
    tests_require=['unittest2', 'mock', 'factory_boy', 'ipdb'],
    classifiers=[
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        'Development Status :: 3 - Alpha',

        'Operating System :: POSIX :: Linux',

        # Specify the Python versions you support here. In particular, ensure
        # that you indicate whether you support Python 2, Python 3 or both.
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
    ],
)
