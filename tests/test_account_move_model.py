import mock
import unittest2 as unittest

from opencell_somconnexio.tryton_models.account_move import AccountMove


class SuperAccountMoveMock(mock.MagicMock):
    _get_origin = mock.MagicMock()


class AccountMovesModelTests(unittest.TestCase):

    @mock.patch("opencell_somconnexio.tryton_models.account_move.super", return_value=SuperAccountMoveMock)
    def test_get_origin_method_return_opencell_invoice(self, SuperMock):
        """ Check if AccountMove _get_origin return the new model OpenCellInvoice. """
        SuperMock.return_value._get_origin.return_value = []
        expected_origins = ['opencell.invoice']

        origins = AccountMove._get_origin()

        self.assertEqual(expected_origins, origins)
