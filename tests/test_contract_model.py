from datetime import datetime, timedelta
import mock
import unittest2 as unittest

from opencell_somconnexio.tryton_models.contract import Contract
from opencell_somconnexio.tryton_tasks import create_contract_in_opencell, update_contracts_in_opencell, finish_contract_in_opencell

from .factories import ContractFactory


class SuperContractMock(mock.MagicMock):
    write = mock.MagicMock()


class ContractModelTests(unittest.TestCase):

    @mock.patch("opencell_somconnexio.tryton_models.contract.delay_task")
    @mock.patch("opencell_somconnexio.tryton_models.contract.super", return_value=SuperContractMock)
    def test_super_write_method_is_called(self, mock1, mock2):
        expected_records = [ContractFactory()]
        expected_values = {
            "contact_email": "some@mail.com",
            "receivable_bank_account": "some-iban"
        }

        Contract.write(expected_records, expected_values)

        SuperContractMock.write.assert_called_with(expected_records, expected_values)

    @mock.patch("opencell_somconnexio.tryton_models.contract.super", return_value=SuperContractMock)
    @mock.patch("opencell_somconnexio.tryton_models.contract.delay_task")
    def test_confirm_contract(self, delay_task_mock, _):
        contract_instance = ContractFactory()
        expected_records = [contract_instance]
        expected_values = {
            "state": "confirmed"
        }

        Contract.write(expected_records, expected_values)

        delay_task_mock.assert_called_with(create_contract_in_opencell, contract_instance.id)

    @mock.patch("opencell_somconnexio.tryton_models.contract.super", return_value=SuperContractMock)
    @mock.patch("opencell_somconnexio.tryton_models.contract.delay_task")
    def test_finish_contract(self, delay_task_mock, _):
        contract_instance = ContractFactory()
        expected_records = [contract_instance]
        expected_values = {
            "state": "finished"
        }

        Contract.write(expected_records, expected_values)

        delay_task_mock.assert_called_with(finish_contract_in_opencell, contract_instance.id)

    @mock.patch("opencell_somconnexio.tryton_models.contract.super", return_value=SuperContractMock)
    @mock.patch("opencell_somconnexio.tryton_models.contract.delay_task")
    def test_update_contracts_in_opencell_is_called_when_updating_email(self, delay_task_mock, _):
        expected_records = [ContractFactory()]
        expected_values = {"contact_email": "some@mail.com"}

        Contract.write(expected_records, expected_values)

        delay_task_mock.assert_called_with(update_contracts_in_opencell, [r.id for r in expected_records], expected_values)

    @mock.patch("opencell_somconnexio.tryton_models.contract.super", return_value=SuperContractMock)
    @mock.patch("opencell_somconnexio.tryton_models.contract.delay_task")
    def test_update_contracts_in_opencell_is_called_when_updating_iban(self, delay_task_mock, _):
        expected_records = [ContractFactory()]
        expected_values = {"receivable_bank_account": "some-iban"}

        Contract.write(expected_records, expected_values)

        delay_task_mock.assert_called_with(update_contracts_in_opencell, [r.id for r in expected_records], expected_values)