import mock
import unittest2 as unittest

from opencell_somconnexio.services.invoice_list_processor import InvoiceListProcessorService

from .opencell_factories import OpenCellRawInvoiceFactory


@mock.patch("opencell_somconnexio.services.invoice_list_processor.InvoiceProcessorService")
class InvoiceListProcessorServiceTests(unittest.TestCase):

    def _invoice_processor_service_mock_side_effect(self, raw_invoice):
        for n in range(self.num_of_invoices):
            if raw_invoice == self.raw_invoices[n]:
                return self.invoice_processor_service_mocks[n]

    def setUp(self):
        self.num_of_invoices = 3
        self.raw_invoices = [OpenCellRawInvoiceFactory() for n in range(self.num_of_invoices)]
        self.invoice_processor_service_mocks = [mock.Mock(spec=["process"]) for n in range(self.num_of_invoices)]

    def test_process_call_to_process_of_every_invoice_processor_service_instance_created_from_raw_invoices_collection(
            self, InvoiceProcessorServiceMock):
        InvoiceProcessorServiceMock.side_effect = self._invoice_processor_service_mock_side_effect

        InvoiceListProcessorService.process(self.raw_invoices)

        for n in range(self.num_of_invoices):
            self.invoice_processor_service_mocks[n].process.assert_called_once()
