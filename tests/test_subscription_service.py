import mock
import unittest2 as unittest
import datetime
from opencell_somconnexio.services.subscription import SubscriptionService
from opencell_somconnexio.exceptions import SubscriptionDoesNotExist, OpenCellConfigurationDoesNotExist
from opencell_somconnexio.opencell_models.services import ContractLineToOCServiceDict

from pyopencell.responses.action_status import ActionStatus

from .factories import ContractFactory


class SubscriptionServiceTests(unittest.TestCase):

    def setUp(self):
        self.contract = ContractFactory()

    @mock.patch("opencell_somconnexio.services.subscription.Subscription")
    def test_subscription_service_terminate(self, SubscriptionMock):
        """ Terminate a subscription corresponding to a Contract instance """

        subscription_instance_mock = mock.MagicMock()
        SubscriptionMock.get().subscription = subscription_instance_mock

        SubscriptionService(self.contract).terminate()

        subscription_instance_mock.terminate.assert_called_with(self.contract.end_date.strftime("%Y-%m-%d"))

    @mock.patch("opencell_somconnexio.services.subscription.Subscription")
    def test_subscription_create_service_applies_one_shot_charge(self, SubscriptionMock):
        """ Create one shot charge in a subscription corresponding to a Contract instance """
        # Any value is OK since we are faking the corresponding method in OpenCellConfiguration factories
        product_template_id = 12
        expected_code = "SOME_ONE_SHOT_CHARGE_CODE"

        subscription_instance_mock = mock.MagicMock()
        SubscriptionMock.get().subscription = subscription_instance_mock

        opencell_configuration_mock = mock.MagicMock()
        opencell_configuration_mock.get_service_code_for_product_template.side_effect = OpenCellConfigurationDoesNotExist(product_template_id)
        opencell_configuration_mock.get_one_shot_charge_code_for_product_template.return_value = expected_code

        for contract_line in self.contract.lines:
            SubscriptionService(self.contract, opencell_configuration_mock).create_service(contract_line, product_template_id)
            subscription_instance_mock.applyOneShotCharge.assert_called_with(expected_code)

    @mock.patch("opencell_somconnexio.services.subscription.Subscription")
    def test_subscription_create_service_activates_a_service(self, SubscriptionMock):
        """ Create and activate services  in a subscripiton corresponding to a Contract instance """
        # Any value is OK since we are faking the corresponding method in OpenCellConfiguration factories
        product_template_id = 12
        expected_code = "SOME_SERVICE_CODE"

        subscription_instance_mock = mock.MagicMock()
        SubscriptionMock.get().subscription = subscription_instance_mock

        opencell_configuration_mock = mock.MagicMock()
        opencell_configuration_mock.get_one_shot_charge_code_for_product_template.side_effect = OpenCellConfigurationDoesNotExist(product_template_id)
        opencell_configuration_mock.get_service_code_for_product_template.return_value = expected_code

        for contract_line in self.contract.lines:
            SubscriptionService(self.contract, opencell_configuration_mock).create_service(contract_line, product_template_id)
            opencell_service = ContractLineToOCServiceDict(contract_line, opencell_configuration_mock).convert()
            subscription_instance_mock.activate.assert_called_with([opencell_service])

    @mock.patch("opencell_somconnexio.services.subscription.Subscription")
    def test_subscription_update_service_termination_date_does_nothing_if_no_termination_date(self, SubscriptionMock):
        """ Does nothing when updates service termination date method receives a termination date = None  """
        subscription_instance_mock = mock.MagicMock()
        SubscriptionMock.get().subscription = subscription_instance_mock

        fake_product_template_id = 69
        SubscriptionService(self.contract, mock.MagicMock()).update_service_termination_date(
            fake_product_template_id, None
        )

        subscription_instance_mock.terminateServices.assert_not_called()

    @mock.patch("opencell_somconnexio.services.subscription.Subscription")
    def test_subscription_update_service_termination_date_does_nothing_can_not_find_service_in_opencell(self, SubscriptionMock):
        """ Does nothing when updates service termination date method if the product template id not has a service code in OC  """
        fake_product_template_id = 69

        subscription_instance_mock = mock.MagicMock()
        SubscriptionMock.get().subscription = subscription_instance_mock

        opencell_configuration_mock = mock.MagicMock()
        opencell_configuration_mock.get_service_code_for_product_template.side_effect = OpenCellConfigurationDoesNotExist(fake_product_template_id)

        SubscriptionService(self.contract, opencell_configuration_mock).update_service_termination_date(
            fake_product_template_id, datetime.datetime.now()
        )

        subscription_instance_mock.terminateServices.assert_not_called()

    @mock.patch("opencell_somconnexio.services.subscription.Subscription")
    def test_subscription_update_service_termination_date_does_nothing_can_not_find_service_in_opencell(self, SubscriptionMock):
        """ Does nothing when updates service termination date method if the product template id not has a service code in OC  """
        expected_code = "SOME_SERVICE_CODE"
        expected_end_date = datetime.datetime.now()
        fake_product_template_id = 69

        subscription_instance_mock = mock.MagicMock()
        SubscriptionMock.get().subscription = subscription_instance_mock

        subscription_instance_mock.services = {
            "serviceInstance": [
                {
                    "code": expected_code,
                }
            ]
        }

        opencell_configuration_mock = mock.MagicMock()
        opencell_configuration_mock.get_service_code_for_product_template.return_value = expected_code

        SubscriptionService(self.contract, opencell_configuration_mock).update_service_termination_date(
            fake_product_template_id, expected_end_date
        )

        subscription_instance_mock.terminateServices.assert_called_with(
            expected_end_date.strftime("%Y-%m-%d"), [expected_code])
