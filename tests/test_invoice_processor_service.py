import mock
import unittest2 as unittest

from opencell_somconnexio.services.invoice_processor import InvoiceProcessorService
from opencell_somconnexio.exceptions import UnsupportedInvoiceType

from .factories import OpenCellInvoiceFactory
from .opencell_factories import OpenCellRawInvoiceFactory
from .pool_mock_proxy import PoolMockProxy


@mock.patch("opencell_somconnexio.services.invoice_processor.Pool", return_value=PoolMockProxy)
class InvoiceProcessorServiceTests(unittest.TestCase):
    def setUp(self):
        self.invoice_id = 44
        self.invoice_number = 34
        self.oc_raw_invoice = OpenCellRawInvoiceFactory(self.invoice_id, self.invoice_number)

    def _opencell_invoice_mock(self, search_return_value=[]):
        opencell_invoice_mock = mock.Mock(spec=["save"])

        def opencell_invoice_side_effect(invoice_id, invoice_number, **kwargs):
            if invoice_id == self.invoice_id and invoice_number == self.invoice_number:
                return opencell_invoice_mock

        PoolMockProxy.OpenCellInvoiceMock = mock.Mock(spec=["search"])
        PoolMockProxy.OpenCellInvoiceMock.search.return_value = search_return_value
        PoolMockProxy.OpenCellInvoiceMock.side_effect = opencell_invoice_side_effect

        return opencell_invoice_mock

    def _account_move(self):
        account_move_mock = mock.Mock(spec=["save"])
        account_move_mock.id = 1

        def account_move_side_effect(**kwargs):
            if "lines" in kwargs.keys():
                return account_move_mock

        PoolMockProxy.AccountMoveMock = mock.Mock(side_effect=account_move_side_effect)
        return account_move_mock

    def _account_move_creator_service_mock(self):
        account_move_creator_mock = mock.Mock(spec=["create"])
        account_move_creator_mock.create.return_value = {"lines": [object]}
        return account_move_creator_mock

    @mock.patch("opencell_somconnexio.services.invoice_processor.logger", return_value=mock.Mock(spec=["info"]))
    def test_process_invoice_already_exist_check_invoice_id(self, logger_mock, pool_mock):
        self._opencell_invoice_mock(search_return_value=[OpenCellInvoiceFactory(self.invoice_id, self.invoice_number)])

        InvoiceProcessorService(self.oc_raw_invoice).process()

        logger_mock.info.assert_called_once_with(
            "Invoice {} already processed".format(self.oc_raw_invoice.invoiceNumber))

    @mock.patch("opencell_somconnexio.services.invoice_processor.logger", return_value=mock.Mock(spec=["info"]))
    def test_process_invoice_without_invoice_number(self, logger_mock, pool_mock):
        self.oc_raw_invoice.invoiceNumber = ""

        InvoiceProcessorService(self.oc_raw_invoice).process()

        logger_mock.info.assert_called_once_with(
            "Invoice ID:{} is not validated".format(self.oc_raw_invoice.invoiceId))

    @mock.patch("opencell_somconnexio.services.invoice_processor.AccountMoveInvoiceCreatorService")
    def test_process_invoice_new_invoice_check_save_call(self, AccountMoveInvoiceCreatorServiceMock, pool_mock):
        opencell_invoice_mock = self._opencell_invoice_mock()
        self._account_move()

        def account_move_creator_side_effect(oc_raw_invoice, oc_invoice):
            if oc_raw_invoice == self.oc_raw_invoice and oc_invoice == opencell_invoice_mock:
                return self._account_move_creator_service_mock()
        AccountMoveInvoiceCreatorServiceMock.side_effect = account_move_creator_side_effect

        InvoiceProcessorService(self.oc_raw_invoice).process()

        opencell_invoice_mock.save.assert_called_once()

    @mock.patch("opencell_somconnexio.services.invoice_processor.AccountMoveInvoiceCreatorService")
    def test_process_invoice_new_invoice_create_account_move_for_COM_invoice(self, AccountMoveInvoiceCreatorServiceMock, pool_mock):
        self._opencell_invoice_mock()
        account_move_mock = self._account_move()

        def account_move_creator_side_effect(oc_raw_invoice, oc_invoice):
            if oc_raw_invoice == self.oc_raw_invoice:
                return self._account_move_creator_service_mock()
        AccountMoveInvoiceCreatorServiceMock.side_effect = account_move_creator_side_effect

        InvoiceProcessorService(self.oc_raw_invoice).process()

        account_move_mock.save.assert_called()

    @mock.patch("opencell_somconnexio.services.invoice_processor.AccountMoveAdjustmentCreatorService")
    def test_process_invoice_new_invoice_create_account_move_for_ADJ_invoice(self, AccountMoveAdjustmentCreatorServiceMock, pool_mock):
        self._opencell_invoice_mock()
        self.oc_raw_invoice = OpenCellRawInvoiceFactory(self.invoice_id, self.invoice_number, invoice_type="ADJ")
        account_move_mock = self._account_move()

        def account_move_creator_side_effect(oc_raw_invoice, oc_invoice):
            if oc_raw_invoice == self.oc_raw_invoice:
                return self._account_move_creator_service_mock()
        AccountMoveAdjustmentCreatorServiceMock.side_effect = account_move_creator_side_effect

        InvoiceProcessorService(self.oc_raw_invoice).process()

        account_move_mock.save.assert_called()

    def test_process_invoice_new_invoice_create_account_move_for_unsupported_invoice_type(self, pool_mock):
        self._opencell_invoice_mock()
        self.oc_raw_invoice = OpenCellRawInvoiceFactory(self.invoice_id, self.invoice_number, invoice_type="NOP")

        with self.assertRaisesRegexp(UnsupportedInvoiceType, self.oc_raw_invoice.invoiceType):
            InvoiceProcessorService(self.oc_raw_invoice).process()
