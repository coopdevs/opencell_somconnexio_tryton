import time
import unittest2 as unittest
from opencell_somconnexio.opencell_models.crm_account_hierarchy import CRMAccountHierarchyFromContract
from opencell_somconnexio.opencell_models.opencell_types.description import Description
from opencell_somconnexio.opencell_models.opencell_types.address import Address

from ..factories import ContractFactory, PartyFactory


class CRMAccountHierarchyFromContractTests(unittest.TestCase):

    def setUp(self):
        self.contract = ContractFactory()
        self.party = self.contract.party
        self.crm_account_hierarchy_code = 0

    # We test the AccountHierarchyResource defined methods in this class
    def test_name_returns_expected_struct(self):
        expected_name = {
            "firstName": self.party.first_name,
            "lastName": self.party.name
        }
        crm_account_hierarchy = CRMAccountHierarchyFromContract(self.contract, self.crm_account_hierarchy_code)

        self.assertEqual(expected_name, crm_account_hierarchy.name)

    def test_description_returns_party_fullname_as_oc_description_type(self):
        expected_description = Description(self.party.full_name).text

        crm_account_hierarchy = CRMAccountHierarchyFromContract(self.contract, self.crm_account_hierarchy_code)

        self.assertEqual(expected_description, crm_account_hierarchy.description)

    def test_address_returns_expected_struct(self):
        expected_address = Address(
            address=self.party.get_contact_address().street,
            zip=self.party.get_contact_address().zip,
            city=self.party.get_contact_address().city,
            state=self.party.get_contact_address().subdivision.name,
            country=self.party.get_contact_address().country.code).to_dict()

        crm_account_hierarchy = CRMAccountHierarchyFromContract(self.contract, self.crm_account_hierarchy_code)

        self.assertEqual(expected_address, crm_account_hierarchy.address)

    def test_vatNo_returns_party_vat_code(self):
        expected_vatNo = self.party.vat_code

        crm_account_hierarchy = CRMAccountHierarchyFromContract(self.contract, self.crm_account_hierarchy_code)

        self.assertEqual(expected_vatNo, crm_account_hierarchy.vatNo)

    def test_email_returns_contract_contact_email(self):
        expected_email = self.contract.contact_email.value

        crm_account_hierarchy = CRMAccountHierarchyFromContract(self.contract, self.crm_account_hierarchy_code)

        self.assertEqual(expected_email, crm_account_hierarchy.email)

    def test_code_returns_crm_account_hierarchy_passed(self):
        crm_account_hierarchy = CRMAccountHierarchyFromContract(self.contract, self.crm_account_hierarchy_code)

        self.assertEqual(self.crm_account_hierarchy_code, crm_account_hierarchy.code)

    def test_crmAccountType_returns_CA_UA(self):
        crm_account_hierarchy = CRMAccountHierarchyFromContract(self.contract, self.crm_account_hierarchy_code)

        self.assertEqual("CA_UA", crm_account_hierarchy.crmAccountType)

    def test_contactInformation_returns_expected_struct(self):
        expected_contact_info = {
            "email": self.contract.contact_email.value,
            "mobile": self.party.get_contact_phone().value,
        }
        crm_account_hierarchy = CRMAccountHierarchyFromContract(self.contract, self.crm_account_hierarchy_code)

        self.assertEqual(expected_contact_info, crm_account_hierarchy.contactInformation)

    def test_contactInformation_returns_expected_struct_with_msidsn_as_phone_if_does_not_have_contact_phone(self):
        # TODO: Fix this hack!!!
        # I can't modify the get_contact_method to force it to return None
        # With this hack, I replace in this test the PartyFactory _phone attribute by the None value
        # At the end of the test, the old value will be returned to the attribute.
        old_phone_method = PartyFactory._phone
        PartyFactory._phone = None

        expected_contact_info = {
            "email": self.contract.contact_email.value,
            "mobile": self.contract.msidsn,
        }
        crm_account_hierarchy = CRMAccountHierarchyFromContract(self.contract, self.crm_account_hierarchy_code)

        self.assertEqual(expected_contact_info, crm_account_hierarchy.contactInformation)

        PartyFactory._phone = old_phone_method

    def test_crmParentCode_returns_party_id(self):
        expected_crmParentCode = self.party.id

        crm_account_hierarchy = CRMAccountHierarchyFromContract(self.contract, self.crm_account_hierarchy_code)

        self.assertEqual(expected_crmParentCode, crm_account_hierarchy.crmParentCode)

    def test_language_returns_ESP(self):
        self.party.lang.code = 'es_ES'

        crm_account_hierarchy = CRMAccountHierarchyFromContract(self.contract, self.crm_account_hierarchy_code)

        self.assertEqual('ESP', crm_account_hierarchy.language)

    def test_language_returns_CAT(self):
        self.party.lang.code = 'ca_ES'

        crm_account_hierarchy = CRMAccountHierarchyFromContract(self.contract, self.crm_account_hierarchy_code)

        self.assertEqual('CAT', crm_account_hierarchy.language)

    def test_language_raises_exeption_if_langage_is_not_knowed(self):
        self.party.lang.code = 'ca_AD'

        crm_account_hierarchy = CRMAccountHierarchyFromContract(self.contract, self.crm_account_hierarchy_code)

        with self.assertRaisesRegexp(Exception, "Can't match tryton's lang_code ca_AD with an OpenCell language code"):
            crm_account_hierarchy.language

    def test_customerCategory_returns_CLIENT(self):
        crm_account_hierarchy = CRMAccountHierarchyFromContract(self.contract, self.crm_account_hierarchy_code)

        self.assertEqual('CLIENT', crm_account_hierarchy.customerCategory)

    def test_currency_returns_EUR(self):
        crm_account_hierarchy = CRMAccountHierarchyFromContract(self.contract, self.crm_account_hierarchy_code)

        self.assertEqual('EUR', crm_account_hierarchy.currency)

    def test_billingCycle_returns_BC_SC_MONTHLY_1ST(self):
        crm_account_hierarchy = CRMAccountHierarchyFromContract(self.contract, self.crm_account_hierarchy_code)

        self.assertEqual('BC_SC_MONTHLY_1ST', crm_account_hierarchy.billingCycle)

    def test_electronicBilling_returns_True(self):
        crm_account_hierarchy = CRMAccountHierarchyFromContract(self.contract, self.crm_account_hierarchy_code)

        self.assertTrue(crm_account_hierarchy.electronicBilling)

    def test_methodOfPayment_returns_expected_struct(self):
        expected_methodOfPayment = [{
            "paymentMethodType": "DIRECTDEBIT",
            "disabled": False,
            "preferred": True,
            "customerAccountCode": self.crm_account_hierarchy_code,
            "bankCoordinates": {
                "iban": self.contract.receivable_bank_account.numbers[0].number_compact,
                "bic": self.contract.receivable_bank_account.bank.bic,
                "accountOwner": self.party.full_name[:50],
                "bankName": self.contract.receivable_bank_account.bank.rec_name,
            },
            "alias": self.contract.receivable_bank_account.id,
            "mandateIdentification": self.contract.receivable_bank_account.id,
            "mandateDate": int(time.mktime(self.contract.receivable_bank_account.create_date.timetuple())),
        }]

        crm_account_hierarchy = CRMAccountHierarchyFromContract(self.contract, self.crm_account_hierarchy_code)

        self.assertEqual(expected_methodOfPayment, crm_account_hierarchy.methodOfPayment)

    def test_methodOfPayment_returns_empty_array_if_contract_does_not_have_receivable_bank_account(self):
        self.contract.receivable_bank_account = None

        expected_methodOfPayment = []

        crm_account_hierarchy = CRMAccountHierarchyFromContract(self.contract, self.crm_account_hierarchy_code)

        self.assertEqual(expected_methodOfPayment, crm_account_hierarchy.methodOfPayment)
