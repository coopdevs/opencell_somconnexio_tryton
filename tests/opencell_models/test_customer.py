import unittest2 as unittest
from opencell_somconnexio.opencell_models.customer import CustomerFromParty
from opencell_somconnexio.opencell_models.opencell_types.description import Description
from opencell_somconnexio.opencell_models.opencell_types.address import Address

from ..factories import PartyFactory


class OpenCellConfigurationFake():
    seller_code = 'SC'
    customer_category_code = 'CLIENT'


class CustomerFromPartyTests(unittest.TestCase):

    def setUp(self):
        self.party = PartyFactory()
        self.opencell_configuration = OpenCellConfigurationFake()

    # We test the AccountHierarchyResource defined methods in this class
    def test_name_returns_expected_struct(self):
        expected_name = {
            "firstName": self.party.first_name,
            "lastName": self.party.name
        }
        customer_from_party = CustomerFromParty(self.party, self.opencell_configuration)

        self.assertEqual(expected_name, customer_from_party.name)

    def test_description_returns_party_fullname_as_oc_description_type(self):
        expected_description = Description(self.party.full_name).text

        customer_from_party = CustomerFromParty(self.party, self.opencell_configuration)

        self.assertEqual(expected_description, customer_from_party.description)

    def test_address_returns_expected_struct(self):
        expected_address = Address(
            address=self.party.get_contact_address().street,
            zip=self.party.get_contact_address().zip,
            city=self.party.get_contact_address().city,
            state=self.party.get_contact_address().subdivision.name,
            country=self.party.get_contact_address().country.code).to_dict()

        customer_from_party = CustomerFromParty(self.party, self.opencell_configuration)

        self.assertEqual(expected_address, customer_from_party.address)

    def test_address_returns_expected_struct_when_dies_not_contact_address_defined(self):
        # TODO: Fix this hack!!!
        # I can't modify the get_contact_address to force it to return None
        # With this hack, I replace in this test the PartyFactory _address attribute by the None value
        # At the end of the test, the old value will be returned to the attribute.
        old_address_method = PartyFactory._address
        PartyFactory._address = None

        expected_address = Address(
            address=self.party.addresses[0].street,
            zip=self.party.addresses[0].zip,
            city=self.party.addresses[0].city,
            state=self.party.addresses[0].subdivision.name,
            country=self.party.addresses[0].country.code).to_dict()

        customer_from_party = CustomerFromParty(self.party, self.opencell_configuration)

        self.assertEqual(expected_address, customer_from_party.address)

        PartyFactory._address = old_address_method

    def test_vatNo_returns_party_vat_code(self):
        expected_vatNo = self.party.vat_code

        customer_from_party = CustomerFromParty(self.party, self.opencell_configuration)

        self.assertEqual(expected_vatNo, customer_from_party.vatNo)

    def test_seller_returns_opencell_configuration_seller_code(self):
        customer_from_party = CustomerFromParty(self.party, self.opencell_configuration)

        self.assertEqual(self.opencell_configuration.seller_code, customer_from_party.seller)

    def test_email_returns_party_contact_email(self):
        expected_email = self.party.get_contact_email().value

        customer_from_party = CustomerFromParty(self.party, self.opencell_configuration)

        self.assertEqual(expected_email, customer_from_party.email)

    def test_code_returns_party_id(self):
        customer_from_party = CustomerFromParty(self.party, self.opencell_configuration)

        self.assertEqual(self.party.id, customer_from_party.code)

    def test_contactInformation_returns_expected_struct(self):
        expected_contact_info = {
            "email": self.party.get_contact_email().value,
            "mobile": self.party.get_contact_phone().value,
        }
        customer_from_party = CustomerFromParty(self.party, self.opencell_configuration)

        self.assertEqual(expected_contact_info, customer_from_party.contactInformation)

    def test_customerCategory_returns_opencell_configuration_customer_category_code(self):
        customer_from_party = CustomerFromParty(self.party, self.opencell_configuration)

        self.assertEqual(self.opencell_configuration.customer_category_code, customer_from_party.customerCategory)
