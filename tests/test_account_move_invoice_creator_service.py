import mock
from datetime import datetime
from decimal import Decimal

from opencell_somconnexio.services.account_move_invoice_creator import AccountMoveInvoiceCreatorService
from .pool_mock_proxy import PoolMockProxy
from .test_account_move_creator_service import AccountMoveCreatorServiceTest


@mock.patch("opencell_somconnexio.services.account_move_creator.Pool", return_value=PoolMockProxy)
class AccountMoveInvoiceCreatorServiceTest(AccountMoveCreatorServiceTest):
    """ Test the AccountMoveInvoiceCreatorService. """

    # AccountMove
    def test_create_account_move_with_correct_company(self, _):
        account_move = AccountMoveInvoiceCreatorService(self.opencell_raw_invoice, self.opencell_invoice).create()

        self.assertEqual(account_move["company"], self.expected_company)

    def test_create_account_move_with_correct_journal(self, _):
        account_move = AccountMoveInvoiceCreatorService(self.opencell_raw_invoice, self.opencell_invoice).create()

        self.assertEqual(account_move["journal"], self.expected_journal)

    def test_create_account_move_with_correct_date(self, _):
        account_move = AccountMoveInvoiceCreatorService(self.opencell_raw_invoice, self.opencell_invoice).create()

        self.assertEqual(account_move["date"], datetime.strptime(self.invoice_date, '%Y-%m-%d').date())

    def test_create_account_move_with_correct_origin(self, _):
        account_move = AccountMoveInvoiceCreatorService(self.opencell_raw_invoice, self.opencell_invoice).create()

        self.assertEqual(account_move["origin"], self.opencell_invoice)

    def test_create_account_move_with_correct_period(self, _):
        account_move = AccountMoveInvoiceCreatorService(self.opencell_raw_invoice, self.opencell_invoice).create()

        self.assertEqual(account_move["period"], self.expected_period)

    def test_create_account_move_with_three_lines(self, _):
        account_move = AccountMoveInvoiceCreatorService(self.opencell_raw_invoice, self.opencell_invoice).create()

        self.assertEqual(len(account_move["lines"]), 3)

    def test_create_account_move_without_tax_line_if_not_tax_code(self, _):
        self.account_tax.invoice_tax_code = None
        account_move = AccountMoveInvoiceCreatorService(self.opencell_raw_invoice, self.opencell_invoice).create()

        self.assertEqual(len(account_move["lines"]), 2)

    # AccountMoveLine Total Amount
    def test_create_account_move_total_amount_line_with_correct_date(self, _):
        account_move = AccountMoveInvoiceCreatorService(self.opencell_raw_invoice, self.opencell_invoice).create()
        total_amount_move_line = account_move["lines"][0]

        self.assertEqual(total_amount_move_line["date"], datetime.strptime(self.invoice_date, '%Y-%m-%d').date())

    def test_create_account_move_total_amount_line_with_correct_party(self, _):
        account_move = AccountMoveInvoiceCreatorService(self.opencell_raw_invoice, self.opencell_invoice).create()
        total_amount_move_line = account_move["lines"][0]

        self.assertEqual(total_amount_move_line["party"], self.party)

    def test_create_account_move_total_amount_line_with_correct_account(self, _):
        account_move = AccountMoveInvoiceCreatorService(self.opencell_raw_invoice, self.opencell_invoice).create()
        total_amount_move_line = account_move["lines"][0]

        self.assertEqual(total_amount_move_line["account"], self.account_client)

    def test_create_account_move_total_amount_line_with_correct_debit(self, _):
        account_move = AccountMoveInvoiceCreatorService(self.opencell_raw_invoice, self.opencell_invoice).create()
        total_amount_move_line = account_move["lines"][0]

        self.assertEqual(total_amount_move_line["debit"], Decimal('36'))

    def test_create_account_move_total_amount_line_with_correct_credit(self, _):
        account_move = AccountMoveInvoiceCreatorService(self.opencell_raw_invoice, self.opencell_invoice).create()
        total_amount_move_line = account_move["lines"][0]

        self.assertEqual(total_amount_move_line["credit"], Decimal('0'))

    # AccountMoveLine Service Line
    def test_create_account_move_service_line_with_correct_date(self, _):
        account_move = AccountMoveInvoiceCreatorService(self.opencell_raw_invoice, self.opencell_invoice).create()
        service_move_line = account_move["lines"][1]

        self.assertEqual(service_move_line["date"], datetime.strptime(self.invoice_date, '%Y-%m-%d').date())

    def test_create_account_move_service_line_with_correct_account(self, _):
        account_move = AccountMoveInvoiceCreatorService(self.opencell_raw_invoice, self.opencell_invoice).create()
        service_move_line = account_move["lines"][1]

        self.assertEqual(service_move_line["account"], self.account_service)

    def test_create_account_move_service_line_with_correct_debit(self, _):
        account_move = AccountMoveInvoiceCreatorService(self.opencell_raw_invoice, self.opencell_invoice).create()
        service_move_line = account_move["lines"][1]

        self.assertEqual(service_move_line["debit"], Decimal('0'))

    def test_create_account_move_service_line_with_correct_credit(self, _):
        account_move = AccountMoveInvoiceCreatorService(self.opencell_raw_invoice, self.opencell_invoice).create()
        service_move_line = account_move["lines"][1]

        self.assertEqual(service_move_line["credit"], Decimal('30'))

    def test_create_account_move_service_line_with_correct_tax_lines(self, _):
        account_move = AccountMoveInvoiceCreatorService(self.opencell_raw_invoice, self.opencell_invoice).create()
        service_move_line = account_move["lines"][1]

        self.assertEqual(len(service_move_line["tax_lines"]), 1)

    # AccountMoveLine Service Line, TaxLines
    def test_create_account_move_service_line_account_tax_line_with_correct_amount(self, _):
        account_move = AccountMoveInvoiceCreatorService(self.opencell_raw_invoice, self.opencell_invoice).create()
        account_tax_line = account_move["lines"][1]["tax_lines"][0]

        self.assertEqual(account_tax_line["amount"], Decimal('30'))

    def test_create_account_move_service_line_account_tax_line_with_correct_code(self, _):
        account_move = AccountMoveInvoiceCreatorService(self.opencell_raw_invoice, self.opencell_invoice).create()
        account_tax_line = account_move["lines"][1]["tax_lines"][0]

        self.assertEqual(account_tax_line["code"], self.account_tax.invoice_base_code)

    def test_create_account_move_service_line_account_tax_line_with_correct_tax(self, _):
        account_move = AccountMoveInvoiceCreatorService(self.opencell_raw_invoice, self.opencell_invoice).create()
        account_tax_line = account_move["lines"][1]["tax_lines"][0]

        self.assertEqual(account_tax_line["tax"], self.account_tax.id)

    # AccountMoveLine Taxes Line
    def test_create_account_move_taxes_line_with_correct_date(self, _):
        account_move = AccountMoveInvoiceCreatorService(self.opencell_raw_invoice, self.opencell_invoice).create()
        taxes_move_line = account_move["lines"][2]

        self.assertEqual(taxes_move_line["date"], datetime.strptime(self.invoice_date, '%Y-%m-%d').date())

    def test_create_account_move_taxes_line_with_correct_account(self, _):
        account_move = AccountMoveInvoiceCreatorService(self.opencell_raw_invoice, self.opencell_invoice).create()
        taxes_move_line = account_move["lines"][2]

        self.assertEqual(taxes_move_line["account"], self.account_taxes)

    def test_create_account_move_taxes_line_with_correct_debit(self, _):
        account_move = AccountMoveInvoiceCreatorService(self.opencell_raw_invoice, self.opencell_invoice).create()
        taxes_move_line = account_move["lines"][2]

        self.assertEqual(taxes_move_line["debit"], Decimal('0'))

    def test_create_account_move_taxes_line_with_correct_credit(self, _):
        account_move = AccountMoveInvoiceCreatorService(self.opencell_raw_invoice, self.opencell_invoice).create()
        taxes_move_line = account_move["lines"][2]

        self.assertEqual(taxes_move_line["credit"], Decimal('6'))

    def test_create_account_move_taxes_line_with_correct_tax_lines(self, _):
        account_move = AccountMoveInvoiceCreatorService(self.opencell_raw_invoice, self.opencell_invoice).create()
        taxes_move_line = account_move["lines"][2]

        self.assertEqual(len(taxes_move_line["tax_lines"]), 1)

    # AccountMoveLine Taxes Line, TaxLines
    def test_create_account_move_account_tax_line_with_correct_amount(self, _):
        account_move = AccountMoveInvoiceCreatorService(self.opencell_raw_invoice, self.opencell_invoice).create()
        account_tax_line = account_move["lines"][2]["tax_lines"][0]

        self.assertEqual(account_tax_line["amount"], Decimal('6'))

    def test_create_account_move_account_tax_line_with_correct_code(self, _):
        account_move = AccountMoveInvoiceCreatorService(self.opencell_raw_invoice, self.opencell_invoice).create()
        account_tax_line = account_move["lines"][2]["tax_lines"][0]

        self.assertEqual(account_tax_line["code"], self.account_tax.invoice_tax_code)

    def test_create_account_move_account_tax_line_with_correct_tax(self, _):
        account_move = AccountMoveInvoiceCreatorService(self.opencell_raw_invoice, self.opencell_invoice).create()
        account_tax_line = account_move["lines"][2]["tax_lines"][0]

        self.assertEqual(account_tax_line["tax"], self.account_tax.id)
