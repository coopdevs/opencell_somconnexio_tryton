import mock
import unittest2 as unittest

from opencell_somconnexio.tryton_models.opencell_tax_codes import OpenCellTaxCodes
from opencell_somconnexio.exceptions import AccountingTaxDoesNotExist


class AccountTaxFake:
    account_tax = object
    pass


@mock.patch("opencell_somconnexio.tryton_models.opencell_tax_codes.OpenCellTaxCodes.search")
class OpenCellTaxCodesModelTest(unittest.TestCase):
    OPENCELL_TAX_CODE = "TAX_HIGH"

    def test_opencell_tax_codes_has_expected_attributes(self, _):
        self.assertTrue(hasattr(OpenCellTaxCodes, "code"))
        self.assertTrue(hasattr(OpenCellTaxCodes, "account_tax"))

    def test_empty_collection_of_account_taxes_returned(self, search_mock):
        """ Raise the AccountingTaxDoesNotExist if the search return a empty collection. """
        search_mock.return_value = []

        with self.assertRaisesRegexp(AccountingTaxDoesNotExist, self.OPENCELL_TAX_CODE):
            OpenCellTaxCodes.get_tax_for_opencell_code(self.OPENCELL_TAX_CODE)

    def test_account_taxe_returned(self, search_mock):
        """ Return a AccounTax instance. """
        account_tax_fake = AccountTaxFake()
        search_mock.return_value = [account_tax_fake]

        self.assertEqual(
            OpenCellTaxCodes.get_tax_for_opencell_code(self.OPENCELL_TAX_CODE),
            account_tax_fake.account_tax)
