import datetime
import factory
from factory import fuzzy


class TrytonModel(object):
    """ Represents Tryton's model """

    def __init__(self, *args, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)


class TrytonModelFactory(factory.Factory):
    """ Generates Tryton's model instances """

    class Meta:
        abstract = True
        model = TrytonModel
        strategy = 'build'

    id = factory.Sequence(lambda n: n)

    @classmethod
    def _create(cls, target_class, *args, **kwargs):
        return target_class(*args, **kwargs)


class OpenCellConfigurationFactory(TrytonModelFactory):
    seller_code = 'SC'
    customer_category_code = 'CLIENT'
    sync_to_opencell = False


class OpenCellServiceCodesFactory(TrytonModelFactory):
    code = 'SE_SC_REC_MOBILE_T_99_1024'


class OpenCellOneShotChargeCodesFactory(TrytonModelFactory):
    code = 'SE_SC_REC_MOBILE_T_99_1024'


class SubdivisionFactory(TrytonModelFactory):
    name = factory.Faker("name")


class CountryFactory(TrytonModelFactory):
    name = factory.Faker("country")
    code = factory.Faker("country_code")


class AddressFactory(TrytonModelFactory):
    street = factory.Faker("address")
    zip = factory.Faker("postcode")
    city = factory.Faker("city")
    subdivision = factory.SubFactory(SubdivisionFactory)
    country = factory.SubFactory(CountryFactory)


class EmailFactory(TrytonModelFactory):
    value = factory.Faker("email")


class PhoneFactory(TrytonModelFactory):
    value = factory.Faker("phone_number")


class LangFactory(TrytonModelFactory):
    code = "es_ES"


class BankAccountNumberFactory(TrytonModelFactory):
    number_compact = factory.Faker("iban")


class BankFactory(TrytonModelFactory):
    bic = "123"
    rec_name = factory.Faker("name")


class BankAccountFactory(TrytonModelFactory):
    # TODO use method to generate it
    numbers = [BankAccountNumberFactory()]
    bank = factory.SubFactory(BankFactory)
    create_date = fuzzy.FuzzyDate(datetime.date(2008, 1, 1))


class ProductTemplateFactory(TrytonModelFactory):
    pass


class ProductFactory(TrytonModelFactory):
    template = factory.SubFactory(ProductTemplateFactory)


class ContractLineFactory(TrytonModelFactory):
    product = factory.SubFactory(ProductFactory)
    start_date = fuzzy.FuzzyDate(datetime.date(2008, 1, 1))

    def current():
        # This method is defined out of this module and we don't want to reproduce
        # the code in this factory. The method always return True, if you want another
        # return value you can mock this method.
        return True


class PartyFactory(TrytonModelFactory):
    _address = AddressFactory()
    _email = EmailFactory()
    _phone = PhoneFactory()
    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')
    full_name = factory.Faker('text')
    name = factory.Faker("last_name")
    vat_code = "03763372C"
    lang = factory.SubFactory(LangFactory)
    addresses = [AddressFactory()]

    def get_contact_address():
        # Always same address for all instances, by now we cannot fix this
        # without touching Contract class, this is out of the scope now.
        # As a workaround, address can be set on build.
        return PartyFactory._address

    def get_contact_email():
        # Always same email for all instances, by now we cannot fix this
        # without touching Contract class, this is out of the scope now.
        # As a workaround, email can be set on build.
        return PartyFactory._email

    def get_contact_phone():
        # Always same address for all instances, by now we cannot fix this
        # without touching Contract class, this is out of the scope now.
        # As a workaround, phone can be set on build.
        return PartyFactory._phone


class ContractFactory(TrytonModelFactory):
    state = "confirmed"
    party = factory.SubFactory(PartyFactory)
    receivable_bank_account = factory.SubFactory(BankAccountFactory)
    msidsn = PhoneFactory().value
    service_type = "mobile"
    start_date = factory.Faker("date_time")
    end_date = factory.Faker("date_time")
    # TODO - use a method instead of this
    contact_email = factory.SubFactory(EmailFactory)
    lines = factory.List([ContractLineFactory()])

    def set_email(self, email):
        self.contact_email = EmailFactory(value=email)


class CustomerFactory(TrytonModelFactory):
    code = factory.Sequence(lambda n: n)

    @factory.lazy_attribute
    def customerAccounts(self):
        return {
            "customerAccount": [{
                "contactInformation": {
                    "email": self.email,
                },
                "methodOfPayment": {
                    "bankCoordinates": {
                        "iban": self.iban
                    }
                }
            }]}


class OpenCellInvoiceFactory:
    def __init__(self,
                 invoice_id=factory.Sequence(lambda n: n),
                 invoice_number=factory.Sequence(lambda n: n),
                 invoice_date=datetime.date(2008, 1, 1)):
        self.invoice_id = invoice_id
        self.invoice_number = invoice_number
        self.invoice_date = invoice_date

    def save(self):
        return self


class CompanyFactory(TrytonModelFactory):
    name = "My Company"


class JournalFactory(TrytonModelFactory):
    code = "REV"


class AccountAccountFactory(TrytonModelFactory):
    def __init__(self, code=None):
        self.code = code if code else factory.Sequence(lambda n: n)


class AccountTaxCodeFactory(TrytonModelFactory):
    pass


class AccountTaxFactory(TrytonModelFactory):
    invoice_tax_code = AccountTaxCodeFactory()
    invoice_base_code = AccountTaxCodeFactory()
    credit_note_tax_code = AccountTaxCodeFactory()
    credit_note_base_code = AccountTaxCodeFactory()

    def __init__(self, invoice_account=None):
        self.invoice_account = invoice_account if invoice_account else AccountAccountFactory()


class PeriodFactory(TrytonModelFactory):
    start_date = factory.Faker("date_time")
    end_date = factory.Faker("date_time")
