from datetime import datetime, timedelta
import mock
import unittest2 as unittest

from opencell_somconnexio.tryton_models.invoice_list_polling import InvoiceListPollingModel
from opencell_somconnexio.opencell_configuration import OpenCellConfiguration


@mock.patch.object(OpenCellConfiguration, "sync_to_opencell", new_callable=mock.PropertyMock(return_value=True))
class InvoiceListPollingModelTests(unittest.TestCase):

    @mock.patch("opencell_somconnexio.tryton_models.invoice_list_polling.InvoiceListPollingService")
    def test_poll_call_poll_method_of_invoice_list_polling_service_with_yesterday_as_filter_date(self, poll_invoice_list_polling_service_mock, sync_to_opencell_mock):
        yesterday = (datetime.today() - timedelta(days=1)).date()
        poll_invoice_list_polling_service_mock.return_value = mock.Mock(spec=["poll"])

        InvoiceListPollingModel.poll()

        poll_invoice_list_polling_service_mock.assert_called_once_with(yesterday)
        poll_invoice_list_polling_service_mock.return_value.poll.assert_called_once()

    @mock.patch("opencell_somconnexio.tryton_models.invoice_list_polling.InvoiceListPollingService")
    def test_poll_call_poll_method_of_invoice_list_polling_service_with_filter_date_passed(self, poll_invoice_list_polling_service_mock, sync_to_opencell_mock):
        filter_date = datetime.today()
        poll_invoice_list_polling_service_mock.return_value = mock.Mock(spec=["poll"])

        InvoiceListPollingModel.poll(filter_date)

        poll_invoice_list_polling_service_mock.assert_called_once_with(filter_date)
        poll_invoice_list_polling_service_mock.return_value.poll.assert_called_once()
