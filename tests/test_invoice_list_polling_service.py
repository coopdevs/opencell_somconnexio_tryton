from datetime import datetime
import mock
import unittest2 as unittest

from opencell_somconnexio.services.invoice_list_polling import InvoiceListPollingService

from opencell_factories import OpenCellInvoiceListFactory, OpenCellRawInvoiceFactory


@mock.patch("opencell_somconnexio.services.invoice_list_polling.InvoiceList.filter_by_date")
@mock.patch("opencell_somconnexio.services.invoice_list_polling.InvoiceListProcessorService", return_date=mock.Mock(spec=["process"]))  # noqa
class InvoiceListPollingServiceTests(unittest.TestCase):

    def test_call_poll_with_pagination_and_process_two_collections_of_invoices(
            self,
            InvoiceListProcessorServiceMock,
            filter_by_date_mock):

        invoices_first_page = [OpenCellRawInvoiceFactory() for n in range(10)]
        invoices_second_page = [OpenCellRawInvoiceFactory() for n in range(3)]

        def filter_by_date_side_effect(date, limit, offset):
            if offset == 0:
                return OpenCellInvoiceListFactory(
                        invoices=invoices_first_page,
                        total_number_of_records=13)
            elif offset == 10:
                return OpenCellInvoiceListFactory(
                        invoices=invoices_second_page,
                        total_number_of_records=13)

        filter_by_date_mock.side_effect = filter_by_date_side_effect

        InvoiceListPollingService(datetime.today()).poll()

        InvoiceListProcessorServiceMock.process.assert_has_calls(
            [mock.call(invoices_first_page), mock.call(invoices_second_page)])
