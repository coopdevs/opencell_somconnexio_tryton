class OpenCellConfigurationDoesNotExist(Exception):
    message = "Can't find OpenCell code corresponding to product template id {}"

    def __init__(self, product_template_id):
        self.message = self.message.format(product_template_id)
        super(OpenCellConfigurationDoesNotExist, self).__init__(self.message)


class SubscriptionDoesNotExist(Exception):
    message = "Can't find OpenCell Subscription instance corresponding to contract id {}"

    def __init__(self, contract_id):
        self.message = self.message.format(contract_id)
        super(SubscriptionDoesNotExist, self).__init__(self.message)


class AccountingTaxDoesNotExist(Exception):
    message = "Can't find AccountTax in Tryton corresponding to OpenCell Tax code {}"

    def __init__(self, opencell_tax_code):
        self.message = self.message.format(opencell_tax_code)
        super(AccountingTaxDoesNotExist, self).__init__(self.message)


class UnsupportedInvoiceType(Exception):
    message = "The invoice with number {} has the type {} that is not supported to be processed"

    def __init__(self, invoice_number, invoice_type):
        self.message = self.message.format(invoice_number, invoice_type)
        super(UnsupportedInvoiceType, self).__init__(self.message)
