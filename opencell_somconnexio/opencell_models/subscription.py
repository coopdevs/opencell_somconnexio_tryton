from opencell_resource import OpenCellResource
from opencell_types.custom_field import CustomField


class SubscriptionFromContract(OpenCellResource):
    white_list = ['code', 'description', 'userAccount', 'offerTemplate', 'subscriptionDate', 'customFields']
    offer_template_code = {
        "mobile": "OF_SC_TEMPLATE_MOB",
        "internet": "OF_SC_TEMPLATE_BA"
    }

    def __init__(self, contract, crm_account_hierarchy_code):
        self.contract = contract
        self.userAccount = crm_account_hierarchy_code

    @property
    def code(self):
        return self.contract.id

    @property
    def description(self):
        return self.contract.msidsn

    @property
    def offerTemplate(self):
        """
        Returns offer template code for current contract's service type.

        :return: offer template code (string)
        """
        return self.offer_template_code[self.contract.service_type]

    @property
    def subscriptionDate(self):
        return self.contract.start_date.strftime("%Y-%m-%d")

    @property
    def customFields(self):
        if self.contract.service_type == 'mobile':
            return {}

        return {
            "customField": [
                CustomField(
                    "CF_OF_SC_SUB_SERVICE_ADDRESS",
                    self.contract.internet_address.street
                ).to_dict(),
                CustomField(
                    "CF_OF_SC_SUB_SERVICE_CP",
                    self.contract.internet_address.zip
                ).to_dict(),
                CustomField(
                    "CF_OF_SC_SUB_SERVICE_CITY",
                    self.contract.internet_address.city
                ).to_dict(),
                CustomField(
                    "CF_OF_SC_SUB_SERVICE_SUBDIVISION",
                    self.contract.internet_address.subdivision.name
                ).to_dict(),
            ]
        }
