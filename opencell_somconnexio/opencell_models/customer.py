from .account_hierarchy_resource import AccountHierarchyResource


class CustomerFromParty(AccountHierarchyResource):

    def __init__(self, party, opencell_configuration):
        self.party = party
        self.white_list = ['seller', 'code', 'description', 'name', 'address', 'vatNo', 'contactInformation',
                           'customerCategory']
        self.opencell_configuration = opencell_configuration

    @property
    def seller(self):
        return self.opencell_configuration.seller_code

    @property
    def code(self):
        return self.party.id

    @property
    def email(self):
        return self.party.get_contact_email().value

    @property
    def customerCategory(self):
        return self.opencell_configuration.customer_category_code

    @property
    def phone(self):
        contact_phone = self.party.get_contact_phone()
        if contact_phone:
            return contact_phone.value
