from datetime import date, datetime

from trytond.pool import Pool


class ContractPresenterFactory:
    """
    Encapsulates specific logic related to Contract and ContractLines that
    the OC integration expects decoupled from the origin Tryton model.
    """
    def __init__(self, id, start_date=None):
        self.id = id
        self.start_date = start_date

    @staticmethod
    def _search_contract(id):
        return Pool().get('contract')(id)

    @staticmethod
    def _set_contract_line_start_date(contract_line, start_date):
        if isinstance(start_date, unicode):
            start_date = datetime.strptime(start_date, "%Y-%m-%dT%H:%M:%S").date()
        if contract_line.start_date < start_date:
            contract_line.start_date = start_date

    @staticmethod
    def _valid_line(line):
        to_activate = line.start_date and date.today() <= line.start_date
        return line.current() or to_activate

    def contract(self):
        contract = self._search_contract(self.id)

        contract.lines = filter(lambda cl: self._valid_line(cl), contract.lines)
        if self.start_date:
            map(lambda cl: self._set_contract_line_start_date(cl, self.start_date), contract.lines)
        return contract
