import logging

from trytond.wizard import Wizard, StateView, StateTransition
from trytond.wizard import Button

from trytond.modules.opencell_somconnexio.opencell_somconnexio.services.invoice_list_polling \
    import InvoiceListPollingService

logger = logging.getLogger(__name__)


class OpenCellInvoicePoll(Wizard):
    """ OpenCell Invoice Poll """
    __name__ = 'opencell.invoice.poll'
    start = StateView(
        'opencell.invoice.poll.start',
        'opencell_somconnexio.opencell_invoice_poll_start',
        [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Continue', 'opencell_invoice_poll', 'tryton-ok', default=True)
        ]
    )
    opencell_invoice_poll = StateTransition()

    def transition_opencell_invoice_poll(self):
        filter_date = self.start.date if self.start.date else None
        InvoiceListPollingService(filter_date=filter_date).poll()
        return 'end'
