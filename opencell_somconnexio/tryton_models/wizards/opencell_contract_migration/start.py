from trytond.model import ModelView, fields


class OpenCellContractMigrationStart(ModelView):
    """ OpenCell Contract Migration start """
    __name__ = 'opencell.contract.migration.start'
    contracts = fields.Many2Many(
        'contract', None, None, 'Contracts', required=True, help="Contracts to be migrated to OC")
    activation_date = fields.Date(
        'Activation Date', required=True,
        help="The OpenCell subscription and related services will be activated on the greatest date between \
            the Tryton Contract activation date and the date provided.")
