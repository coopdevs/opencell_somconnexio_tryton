import logging
from trytond.model import ModelSingleton, ModelView, ModelSQL, fields


logger = logging.getLogger(__name__)


class OpenCellConfiguration(ModelSingleton, ModelView, ModelSQL):
    """OpenCell Configuration"""

    __name__ = 'opencell.configuration'

    seller_code = fields.Char('Seller Code', required=False)
    customer_category_code = fields.Char('Customer category code', required=False)
    sync_to_opencell = fields.Boolean('Sync to OpenCell')

    @classmethod
    def default_sync_to_opencell(cls):
        return False
