from datetime import datetime, timedelta
from trytond.model import Model

from ..tryton_tasks import run_task
from ..services.invoice_list_polling import InvoiceListPollingService


class InvoiceListPollingModel(Model):
    """ This method is to be called from a TrytonCron.  TrytonCron needs a Model with a classmethod """
    __name__ = 'invoice.list.polling'

    @classmethod
    def poll(cls, filter_date=None):
        filter_date = cls._filter_date(filter_date)
        run_task(InvoiceListPollingService(filter_date).poll)

    @classmethod
    def _filter_date(cls, filter_date):
        return filter_date if filter_date else (datetime.today() - timedelta(days=1)).date()
