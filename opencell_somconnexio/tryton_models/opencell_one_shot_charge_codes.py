import logging
from trytond.model import ModelView, ModelSQL, fields, Unique
from ..exceptions import OpenCellConfigurationDoesNotExist


logger = logging.getLogger(__name__)


class OpenCellOneShotChargeCodes(ModelView, ModelSQL):
    """OpenCell One Shot Charges"""

    __name__ = 'opencell.one_shot_charge_codes'

    product_template = fields.Many2One(
        'product.template', 'Product', required=True)
    code = fields.Char('Opencell Code', required=True)

    @classmethod
    def __setup__(cls):
        super(OpenCellOneShotChargeCodes, cls).__setup__()
        table = cls.__table__()
        cls._sql_constraints = [
            ('product_template_unique', Unique(table, table.product_template),
             'Product template must be unique'),
        ]

    @classmethod
    def get_code_for_product_template(cls, product_template_id):
        opencell_one_shot_charge_code_items = cls.search([
            ('product_template', '=', product_template_id),
        ])

        if len(opencell_one_shot_charge_code_items) > 1:
            logger.warning('Several OpenCell codes found for same product template id ({})'.format(product_template_id))

        if not opencell_one_shot_charge_code_items:
            raise OpenCellConfigurationDoesNotExist(product_template_id)

        return opencell_one_shot_charge_code_items[0].code
