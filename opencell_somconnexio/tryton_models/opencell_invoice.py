import logging

from trytond.model import ModelView, ModelSQL, fields, Unique

logger = logging.getLogger(__name__)


_INVOICE_TYPES = [
    ('COM', 'Commercial Invoice'),
    ('ADJ', 'Adjustment Invoice')]


class OpenCellInvoice(ModelView, ModelSQL):
    """OpenCell Invoice"""

    __name__ = 'opencell.invoice'

    invoice_id = fields.Integer('OpenCell ID', required=True, readonly=True)
    invoice_number = fields.Char('OpenCell Invoice Number', required=True, readonly=True)
    invoice_date = fields.Date('Invoice Date', required=True, readonly=True)
    invoice_type = fields.Selection(_INVOICE_TYPES, 'Invoice Type', required=True, readonly=True)
    amount_without_tax = fields.Numeric('Untaxed Amount', digits=(16, 4), required=True, readonly=True)
    tax_amount = fields.Numeric('Tax amount', digits=(16, 4), required=True, readonly=True)
    amount_with_tax = fields.Numeric('Taxed Amount', digits=(16, 4), required=True, readonly=True)

    @classmethod
    def __setup__(cls):
        super(OpenCellInvoice, cls).__setup__()
        table = cls.__table__()
        cls._sql_constraints = [
            ('invoice_id_unique', Unique(table, table.invoice_id), 'Invoice ID must be unique'),
            ('invoice_number_unique', Unique(table, table.invoice_number), 'Invoice Number must be unique'),
        ]

    def get_rec_name(self, name):
        return self.invoice_number
