import logging

from sql.aggregate import Min
from sql.operators import And, Concat, Like

from trytond.model import ModelSQL, ModelView, fields
from trytond.pool import Pool

from opencell_invoice import _INVOICE_TYPES

logger = logging.getLogger(__name__)


class OpenCellInvoiceTaxesAccountMoveReport(ModelSQL, ModelView):
    """OpenCell Invoice Taxes Account Move Report"""

    __name__ = 'opencell.invoice.taxes.account.move.report'

    debit = fields.Numeric('Debit')
    credit = fields.Numeric('Credit')
    account = fields.Many2One('account.account', 'Account')
    party = fields.Many2One('party.party', 'Party')
    reconciliation = fields.Many2One('account.move.reconciliation', 'Reconciliation')
    move = fields.Many2One('account.move', 'Move')
    period = fields.Many2One('account.period', 'Period')
    date = fields.Date('Date')
    invoice_id = fields.Integer('Invoice ID')
    invoice_number = fields.Char('Invoice Number')
    invoice_date = fields.Date('Invoice Date')
    invoice_type = fields.Selection(_INVOICE_TYPES, 'Invoice Type')
    invoice_amount_without_tax = fields.Numeric('Invoice Untaxed Amount')
    invoice_tax_amount = fields.Numeric('Invoice Tax amount')
    invoice_amount_with_tax = fields.Numeric('Invoice Taxed Amount')

    @classmethod
    def table_query(cls):
        pool = Pool()
        AccountMove = pool.get('account.move')
        AccountAccount = pool.get('account.account')
        AccountMoveLine = pool.get('account.move.line')
        OpenCellInvoice = pool.get('opencell.invoice')

        account_move = AccountMove.__table__()
        account_account = AccountAccount.__table__()
        account_move_line = AccountMoveLine.__table__()
        opencell_invoice = OpenCellInvoice.__table__()

        account_move_date = account_move.date.cast(cls.date.sql_type().base)
        opencell_invoice_date = opencell_invoice.invoice_date.cast(cls.date.sql_type().base)

        join = account_account.join(account_move_line, condition=And([
            Like(account_account.code, '47%'),
            account_move_line.account == account_account.id]))
        join = join.join(account_move, condition=account_move_line.move == account_move.id)
        join = join.join(
            opencell_invoice,
            condition=account_move.origin == Concat('opencell.invoice,', opencell_invoice.id))
        return join.select(
            Min(account_move_line.id).as_('id'),
            Min(account_move_line.create_date).as_('create_date'),
            Min(account_move_line.write_date).as_('write_date'),
            Min(account_move_line.create_uid).as_('create_uid'),
            Min(account_move_line.write_uid).as_('write_uid'),
            account_move_line.party.as_('party'),
            account_move_date.as_('date'),
            account_move_line.credit.as_('credit'),
            account_move_line.debit.as_('debit'),
            account_move_line.account.as_('account'),
            account_move_line.move.as_('move'),
            account_move.period.as_('period'),
            account_move_line.reconciliation.as_('reconciliation'),
            opencell_invoice.invoice_id.as_('invoice_id'),
            opencell_invoice.invoice_number.as_('invoice_number'),
            opencell_invoice_date.as_('invoice_date'),
            opencell_invoice.invoice_type.as_('invoice_type'),
            opencell_invoice.amount_without_tax.as_('invoice_amount_without_tax'),
            opencell_invoice.tax_amount.as_('invoice_tax_amount'),
            opencell_invoice.amount_with_tax.as_('invoice_amount_with_tax'),
            group_by=[
                account_move_line.party,
                account_move_date,
                account_move_line.credit,
                account_move_line.debit,
                account_move_line.account,
                account_move_line.move,
                account_move.period,
                account_move_line.reconciliation,
                opencell_invoice.invoice_id,
                opencell_invoice.invoice_number,
                opencell_invoice_date,
                opencell_invoice.invoice_type,
                opencell_invoice.amount_without_tax,
                opencell_invoice.tax_amount,
                opencell_invoice.amount_with_tax,
            ])
