import logging

from ..services.account_move_creator import AccountMoveCreatorService

logger = logging.getLogger(__name__)


class AccountMoveInvoiceCreatorService(AccountMoveCreatorService):
    """
    Call to create returns a dict with the data to create an AccountMove of an Invoice
    """
    def _total_amount_with_tax_move_line(self):
        """ Accounting move line with the total amount with tax as debit """
        return [{
                "party": self._party(),
                "account": self._account_account("43000000"),
                "credit": self._amount(0),
                "debit": self._amount(self.opencell_raw_invoice.amountWithTax),
                "date": self.opencell_invoice.invoice_date
                }]

    def _service_account_move_line_data(self, subcategory_invoice_agregate):
        amount = self._amount(subcategory_invoice_agregate.get("amountWithoutTax"))
        if amount == 0:
            return {}

        tax = self.OpenCellTaxCodes.get_tax_for_opencell_code(subcategory_invoice_agregate.get("taxCode"))
        return {
            "account": self._account_account(subcategory_invoice_agregate.get("accountingCode")),
            "credit": amount,
            "debit": self._amount(0),
            "date": self.opencell_invoice.invoice_date,
            "tax_lines": self._account_tax_lines(tax.id, amount, tax.invoice_base_code)
        }

    def _tax_account_move_line_data(self, tax_line):
        tax = self.OpenCellTaxCodes.get_tax_for_opencell_code(tax_line.get("taxCode"))
        amount = self._amount(tax_line["amountTax"])
        if not tax.invoice_tax_code:
            return {}

        return {
            "account": tax.invoice_account,
            "credit": amount,
            "debit": self._amount(0),
            "date": self.opencell_invoice.invoice_date,
            "tax_lines": self._account_tax_lines(tax.id, amount, tax.invoice_tax_code)
        }
