import logging
import math

from pyopencell.resources.invoice_list import InvoiceList

from ..services.invoice_list_processor import InvoiceListProcessorService

logger = logging.getLogger(__name__)


class InvoiceListPollingService():
    """ Manage the polling to retrieve invoices from OC

    :param filter_date: A date to retrieve the invoices of this concrete date. Default is yesterday.
    """

    def __init__(self, filter_date):
        self.filter_date = filter_date
        self.items_per_page = 10
        self.continue_polling = True
        self.page = 0  # From 0 to N-1

    @property
    def offset(self):
        return self.page * self.items_per_page

    def _number_of_pages(self):
        return int(math.ceil(float(self.total_number_of_records) / self.items_per_page))

    def _polling_pagination(self):
        """ Check the pagination and return if exist another page. """
        self.page += 1
        self.continue_polling = self.page < self._number_of_pages()

    def _poll_and_process(self):
        """ Poll invoices from OC and call the processor to process them. """
        invoice_list = InvoiceList.filter_by_date(
            date=self.filter_date,
            limit=self.items_per_page,
            offset=self.offset)
        InvoiceListProcessorService.process(invoice_list.invoices)
        self.total_number_of_records = invoice_list.paging.totalNumberOfRecords

    def poll(self):
        """ Manage the polling process. """
        logger.info("Start polling invoice process...")

        while self.continue_polling:
            self._poll_and_process()
            self._polling_pagination()

        logger.info("End polling invoice process...")
